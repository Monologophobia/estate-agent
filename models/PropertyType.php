<?php namespace Monologophobia\EstateAgent\Models;

use \October\Rain\Database\Model;

class PropertyType extends Model {

    public $table = 'mono_ea_property_types';

    public $timestamps = true;

    // Any validation for incoming data
    use \October\Rain\Database\Traits\Validation;
    public $rules = ['name' => 'required|string',];

    public $hasMany = [
        'properties' => ['Monologophobia\EstateAgent\Models\Property', 'key' => 'property_type_id']
    ];

}
