<?php namespace Monologophobia\EstateAgent\Models;

use \October\Rain\Database\Model;

class PropertyStatus extends Model {

    public $table = 'mono_ea_property_statuses';

    public $timestamps = true;

    // Any validation for incoming data
    use \October\Rain\Database\Traits\Validation;
    public $rules = ['name' => 'required|string',];

    public $hasMany = [
        'properties' => ['Monologophobia\EstateAgent\Models\Property', 'key' => 'property_status_id', 'delete' => true]
    ];

}
