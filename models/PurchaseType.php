<?php namespace Monologophobia\EstateAgent\Models;

use \October\Rain\Database\Model;

class PurchaseType extends Model {

    public $table = 'mono_ea_purchase_types';

    public $timestamps = true;

    // Any validation for incoming data
    use \October\Rain\Database\Traits\Validation;
    public $rules = ['name' => 'required|string',];

    public $hasMany = [
        'properties' => ['Monologophobia\EstateAgent\Models\Property', 'key' => 'purchase_type_id']
    ];

}
