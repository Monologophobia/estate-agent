<?php namespace Monologophobia\EstateAgent\Models;

use \October\Rain\Database\Model;

use Monologophobia\EstateAgent\Models\Settings;
use Monologophobia\EstateAgent\Models\PaymentType;
use Monologophobia\EstateAgent\Models\PurchaseType;
use Monologophobia\EstateAgent\Models\PropertyType;

class Property extends Model {

    use \October\Rain\Database\Traits\Nullable;
    use \October\Rain\Database\Traits\Validation;

    public $table = 'mono_ea_properties';

    public $timestamps = true;
    protected $jsonable = ['features'];
    protected $nullable = ['landlord_id', 'features', 'price'];

    // Any validation for incoming data
    use \October\Rain\Database\Traits\Validation;
    public $rules = [
        'name'        => 'required|string',
        'description' => 'required|string',
        'postcode'    => 'required|string'
    ];

    // Relationships
    public $belongsTo = [
        'landlord'        => ['Monologophobia\EstateAgent\Models\Landlord', 'key' => 'landlord_id'],
        'property_type'   => ['Monologophobia\EstateAgent\Models\PropertyType', 'key' => 'property_type_id'],
        'purchase_type'   => ['Monologophobia\EstateAgent\Models\PurchaseType', 'key' => 'purchase_type_id'],
        'payment_type'    => ['Monologophobia\EstateAgent\Models\PaymentType', 'key' => 'payment_type_id'],
        'property_status' => ['Monologophobia\EstateAgent\Models\PropertyStatus', 'key' => 'property_status_id'],
    ];

    public $belongsToMany = [
        'searchable_features' => [
            'Monologophobia\EstateAgent\Models\SearchableFeature',
            'table'    => 'mono_ea_propertys_features',
            'key'      => 'property_id',
            'otherKey' => 'feature_id',
        ]
    ];

    public $attachOne = [
        'featured_image' => ['System\Models\File', 'delete' => true]
    ];

    public $attachMany = [
        'images' => ['System\Models\File', 'delete' => true]
    ];

    public function getLandlordOptions() {
        $array = [0 => "None"];
        foreach (Landlord::get() as $landlord) $array[$landlord->id] = $landlord->name;
        return $array;
    }

    public function isNew() {
        $created_at    = strtotime($this->created_at);
        $one_month_ago = strtotime('-1 month');
        if ($created_at > $one_month_ago) return true;
        return false;
    }

    public function beforeSave() {
        // if we haven't been supplied a latitude and longitude, look it up from the address and postcode
        if (!$this->latitude || !$this->longitude) {
            $address = $this->address . " " . $this->postcode;
            $lat_long = Property::getLatLongByAddress($address, $this->postcode);
            $this->latitude = $lat_long['latitude'];
            $this->longitude = $lat_long['longitude'];
        }
    }

    /**
     * Returns a collection of Properties ordered by distance from location
     * @param String Location
     * @return Property Collection
     */
    public static function getByLocation($location) {
        $lat_long = Property::getLatLongByAddress($location);
        $properties = Property::get();
        foreach ($properties as $property) {
            $property->distance = $property->distanceFrom($lat_long['latitude'], $lat_long['longitude']);
        }
        return $properties->sortBy('distance');
    }

    /**
     * Gets a latitude and longitude by a supplied address
     * Uses the Google Maps API so an API key will be required
     * @param Text address - Can be a partial or full
     * @return Array ['latitude', 'longitude']
     */
    public static function getLatLongByAddress($address, $postcode = false) {

        $location_region = Settings::get('location_region');
        $address         = str_replace(' ', '+', $location_region) . '+' . str_replace(' ', '+', $address);

        if (Settings::get('location_provider', 'geocode') == 'google') {

            $key = Settings::get('google_maps_api_key');
            if (!$key) throw new \Exception ("No Google Maps API Key supplied");

            $geocode = file_get_contents('https://maps.google.com/maps/api/geocode/json?address=' . $address . '&sensor=false&key=' . $key);
            $output  = json_decode($geocode);

            if (!isset($output->results) || count($output->results) <= 0) {
                throw new \Exception ("No Location Found");
            }

            $location = $output->results[0]->geometry->location;

            return ['latitude' => $location->lat, 'longitude' => $location->lng];

        }
        else {
            $location = rawurlencode($postcode);
            $geocode  = file_get_contents('https://geocode.xyz/' . $postcode . "?geoit=JSON&region=uk");
            $output   = json_decode($geocode);
            if (isset($output->error)) throw new \Exception($output->error->description);
            return ['latitude' => $output->latt, 'longitude' => $output->longt];
        }

    }

    /**
     * Calculates the distance of this property from the supplied latitude and longitude
     * Adapted from https://www.geodatasource.com/developers/php
     * @param Latitude
     * @param Longitude
     */
    public function distanceFrom($latitude, $longitude) {
        $theta    = $this->longitude - $longitude;
        $distance = sin(deg2rad(floatval($this->latitude))) * 
                    sin(deg2rad(floatval($latitude))) +
                    cos(deg2rad(floatval($this->latitude))) *
                    cos(deg2rad(floatval($latitude))) *
                    cos(deg2rad(floatval($theta)));
        $distance = acos($distance);
        $distance = rad2deg($distance);
        // return miles
        return $distance * 60 * 1.1515;
    }

}
