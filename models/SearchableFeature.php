<?php namespace Monologophobia\EstateAgent\Models;

use \October\Rain\Database\Model;

class SearchableFeature extends Model {

    // The table to use
    public $table = 'mono_ea_features';

    public $timestamps = true;

    // Any validation for incoming data
    use \October\Rain\Database\Traits\Validation;
    public $rules = ['name' => 'required|string',];

    public $belongsToMany = [
        'properties' => [
            'Monologophobia\EstateAgent\Models\SearchableFeature',
            'table'    => 'mono_ea_propertys_features',
            'key'      => 'featured_id',
            'otherKey' => 'property_id',
        ]
    ];

}
