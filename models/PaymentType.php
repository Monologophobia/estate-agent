<?php namespace Monologophobia\EstateAgent\Models;

use \October\Rain\Database\Model;

class PaymentType extends Model {

    public $table = 'mono_ea_payment_types';

    public $timestamps = true;

    // Any validation for incoming data
    use \October\Rain\Database\Traits\Validation;
    public $rules = ['name' => 'required|string',];

    public $hasMany = [
        'properties' => ['Monologophobia\EstateAgent\Models\Property', 'key' => 'payment_type_id']
    ];

}
