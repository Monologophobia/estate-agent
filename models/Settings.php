<?php namespace Monologophobia\EstateAgent\Models;

use Model;

class Settings extends Model {

    public $implement = ['System.Behaviors.SettingsModel'];

    // A unique code
    public $settingsCode = 'mono_ea_settings';

    // Reference to field configuration
    public $settingsFields = 'fields.yaml';

}
