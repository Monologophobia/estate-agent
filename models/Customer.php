<?php namespace Monologophobia\EstateAgent\Models;

use \October\Rain\Database\Model;

class Customer extends Model {

    // The table to use
    public $table = 'mono_ea_customers';

    // Automatically generate created_at and updated_at
    public $timestamps = true;
    use \October\Rain\Database\Traits\SoftDelete;
    protected $dates = ['deleted_at'];

    // Any validation for incoming data
    use \October\Rain\Database\Traits\Validation;
    public $rules = [
        'name'    => 'required|string',
        'contact' => 'required|string'
    ];

}
