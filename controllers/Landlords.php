<?php namespace Monologophobia\EstateAgent\Controllers;

use Flash;
use BackendMenu;
use Backend\Classes\Controller;
use Monologophobia\EstateAgent\Models\Landlord;

class Landlords extends \Backend\Classes\Controller {

    public $implement = [
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.FormController'
    ];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public function __construct() {
        parent::__construct();
        BackendMenu::setContext('Monologophobia.EstateAgent', 'estateagent', 'landlords');
    }

    public function index_onDelete() {
        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {
            foreach ($checkedIds as $id) {
                $custom_field = Landlord::findOrFail($id);
                $custom_field->delete();
            }
            Flash::success('Deleted Successfully');
        }
        else {
            Flash::error('Couldn\'t find the IDs');
        }
        return $this->listRefresh();
    }

}