<?php namespace Monologophobia\EstateAgent\Controllers;

use Flash;
use BackendMenu;
use Backend\Classes\Controller;
use Monologophobia\EstateAgent\Models\SearchableFeature;

class SearchableFeatures extends \Backend\Classes\Controller {

    public $implement = [
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.FormController'
    ];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public function __construct() {
        parent::__construct();
        BackendMenu::setContext('Monologophobia.EstateAgent', 'estateagent', 'properties');
    }

    public function index_onDelete() {
        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {
            foreach ($checkedIds as $id) {
                $record = SearchableFeature::findOrFail($id);
                $record->delete();
            }
            Flash::success('Deleted Successfully');
        }
        else {
            Flash::error('Couldn\'t find the IDs');
        }
        return $this->listRefresh();
    }

}