<?php namespace Monologophobia\EstateAgent\Updates;

use DB;
use Schema;
use October\Rain\Database\Updates\Migration;

class ZeroPointThreePointZero extends Migration {

    public function up() {

        Schema::table('mono_ea_properties', function($table) {
            $table->text('short_description')->nullable();
            $table->integer('sort_order')->default(0);
        });

    }

    public function down() {
        Schema::table('mono_ea_properties', function($table) {
            $table->dropColumn('short_description');
            $table->dropColumn('sort_order');
        });
    }

}
