<?php namespace Monologophobia\EstateAgent\Updates;

use DB;
use Schema;
use October\Rain\Database\Updates\Migration;

class CreateTables extends Migration {

    public function up() {

        Schema::create('mono_ea_landlords', function($table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->text('name');
            $table->text('contact')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        DB::table('mono_ea_landlords')->insert([
            'id' => 1,
            'name' => 'Default',
        ]);

        Schema::create('mono_ea_property_types', function($table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->text('name');
            $table->integer('sort_order')->default(0);
            $table->timestamps();
        });

        DB::table('mono_ea_property_types')->insert([
            'id'   => 1,
            'name' => "House",
        ]);

        Schema::create('mono_ea_purchase_types', function($table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->text('name');
            $table->integer('sort_order')->default(0);
            $table->timestamps();
        });

        DB::table('mono_ea_purchase_types')->insert([
            'id'   => 1,
            'name' => "Sale",
        ]);
        DB::table('mono_ea_purchase_types')->insert([
            'id'   => 2,
            'name' => "Letting",
        ]);

        Schema::create('mono_ea_payment_types', function($table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->text('name');
            $table->integer('sort_order')->default(0);
            $table->timestamps();
        });

        DB::table('mono_ea_payment_types')->insert([
            'id'   => 1,
            'name' => "One-Off",
        ]);
        DB::table('mono_ea_payment_types')->insert([
            'id'   => 2,
            'name' => "Per Month",
        ]);
        DB::table('mono_ea_payment_types')->insert([
            'id'   => 3,
            'name' => "Per Week",
        ]);

        Schema::create('mono_ea_properties', function($table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->text('name');
            $table->text('description');
            $table->integer('purchase_type_id')->index()->unsigned()->nullable();
            $table->foreign('purchase_type_id')->references('id')->on('mono_ea_purchase_types')->onDelete('set null');
            $table->integer('landlord_id')->index()->unsigned()->nullable();
            $table->foreign('landlord_id')->references('id')->on('mono_ea_landlords')->onDelete('cascade');
            $table->integer('property_type_id')->index()->unsigned()->nullable();
            $table->foreign('property_type_id')->references('id')->on('mono_ea_property_types')->onDelete('set null');
            $table->text('address')->nullable();
            $table->string('postcode');
            $table->decimal('latitude', 9, 6)->nullable();
            $table->decimal('longitude', 9, 6)->nullable();
            $table->integer('bedrooms')->default(1);
            $table->integer('bathrooms')->default(1);
            $table->decimal('price', 8, 2)->nullable();
            $table->integer('payment_type_id')->index()->unsigned()->nullable();
            $table->foreign('payment_type_id')->references('id')->on('mono_ea_payment_types')->onDelete('cascade');
            $table->json('features')->nullable();
            $table->boolean('is_available')->default(true);
            $table->timestamps();
        });

        Schema::create('mono_ea_features', function($table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->text('name');
            $table->integer('sort_order')->default(0);
            $table->timestamps();
        });

        $features = [
            1 => "Garden",
            2 => "Parking",
            3 => "New Build",
            4 => "Retirement Home",
        ];
        foreach ($features as $id => $feature) {
            DB::table('mono_ea_features')->insert([
                'id'   => $id,
                'name' => $feature,
            ]);
        }

        Schema::create('mono_ea_propertys_features', function($table) {
            $table->integer('property_id')->unsigned();
            $table->integer('feature_id')->unsigned();
        });

    }

    public function down() {
        Schema::dropIfExists('mono_ea_propertys_features');
        Schema::dropIfExists('mono_ea_features');
        Schema::dropIfExists('mono_ea_properties');
        Schema::dropIfExists('mono_ea_purchase_types');
        Schema::dropIfExists('mono_ea_property_types');
        Schema::dropIfExists('mono_ea_payment_types');
        Schema::dropIfExists('mono_ea_landlords');
    }

}
