<?php namespace Monologophobia\EstateAgent\Updates;

use DB;
use Schema;
use October\Rain\Database\Updates\Migration;

class ZeroPointTwoPointZero extends Migration {

    public function up() {

        Schema::create('mono_ea_property_statuses', function($table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->text('name');
            $table->timestamps();
        });

        $statuses = [
            1 => "For Sale",
            2 => "To Rent",
            3 => "Under Offer",
            4 => "Sold STC",
            5 => "Sold",
            6 => "Let",
        ];
        foreach ($statuses as $id => $status) {
            DB::table('mono_ea_property_statuses')->insert([
                'id'   => $id,
                'name' => $status,
            ]);
        }

        Schema::table('mono_ea_properties', function($table) {
            $table->dropColumn('is_available');
            $table->integer('property_status_id')->index()->unsigned()->default(1);
            $table->foreign('property_status_id')->references('id')->on('mono_ea_property_statuses')->onDelete('cascade');
        });

    }

    public function down() {
        Schema::table('mono_ea_properties', function($table) {
            $table->boolean('is_available')->default(true);
            $table->dropForeign(['property_status_id']);
            $table->dropColumn('property_status_id');
        });
    }

}
