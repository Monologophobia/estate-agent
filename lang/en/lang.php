<?php

return [
    'plugin' => [
        'name'          => 'Estate Agent',
        'description'   => 'Property Management System',
        'icon'          => 'icon-home'
    ],
    'tabs' => [
        'properties' => [
            'name' => 'Properties',
            'icon' => 'icon-home'
        ],
        'landlords' => [
            'name' => 'Landlords',
            'icon' => 'icon-briefcase'
        ],
        'customers' => [
            'name' => 'Customers',
            'icon' => 'icon-users'
        ]
    ]
];
