<?php namespace Monologophobia\EstateAgent\Components;

use Redirect;

use Monologophobia\EstateAgent\Models\Property;
use Monologophobia\EstateAgent\Models\Settings;

class Properties extends \Cms\Classes\ComponentBase {

    public function componentDetails() {
        return [
            'name' => 'Search Properties',
            'description' => 'Displays properties by address or displays a search box'
        ];
    }

    public function defineProperties() {
        return [
            'id' => [
                 'title' => 'Property ID',
                 'type'  => 'string'
            ]
        ];
    }

    public function onRun() {

        try {

            // if this is a specific property
            if ($id = intval($this->property('id'))) {
                $this->addJs("https://unpkg.com/leaflet@1.6.0/dist/leaflet.js");
                $this->addCss("https://unpkg.com/leaflet@1.7.1/dist/leaflet.css");
                $this->addJs('https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js');
                $this->addCss('https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css');
                $this->page['property'] = Property::findOrFail($id);
            }

            // else we're searching for properties in the location of
            else if ($location = post('location')) {
                // honeypot
                if (!empty(post('email'))) return false;
                $this->page['lat_long'] = Property::getLatLongByAddress($location);
                $this->page['location'] = $location;
                $this->page['properties'] = Property::getByLocation($location);
            }

        }
        catch (\Exception $e) {
            return Redirect::refresh()->with('error', $e->getMessage());
        }

    }

}
