<?php namespace Monologophobia\EstateAgent;

use Lang;

use Backend\Facades\Backend;
use System\Classes\PluginBase;

/**
 * Estate Agent Plugin Information File
 */
class Plugin extends PluginBase {

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails() {
        return [
            'name'        => Lang::get('monologophobia.estateagent::lang.plugin.name'),
            'description' => Lang::get('monologophobia.estateagent::lang.plugin.description'),
            'author'      => 'Monologophobia',
            'icon'        => Lang::get('monologophobia.estateagent::lang.plugin.icon')
        ];
    }

    // Register plugin permissions
    public function registerPermissions() {

    }

    // Create the backend navigation
    public function registerNavigation() {
        return [
            'estateagent' => [
                'label'       => Lang::get('monologophobia.estateagent::lang.plugin.name'),
                'url'         => Backend::url('monologophobia/estateagent/properties'),
                'icon'        => Lang::get('monologophobia.estateagent::lang.plugin.icon'),
                'order'       => 601,

                'sideMenu' => [
                    'properties' => [
                        'label'       => Lang::get('monologophobia.estateagent::lang.tabs.properties.name'),
                        'url'         => Backend::url('monologophobia/estateagent/properties'),
                        'icon'        => Lang::get('monologophobia.estateagent::lang.tabs.properties.icon')
                    ],
                    'landlords' => [
                        'label'       => Lang::get('monologophobia.estateagent::lang.tabs.landlords.name'),
                        'url'         => Backend::url('monologophobia/estateagent/landlords'),
                        'icon'        => Lang::get('monologophobia.estateagent::lang.tabs.landlords.icon')
                    ],
                    'customers' => [
                        'label'       => Lang::get('monologophobia.estateagent::lang.tabs.customers.name'),
                        'url'         => Backend::url('monologophobia/estateagent/customers'),
                        'icon'        => Lang::get('monologophobia.estateagent::lang.tabs.customers.icon')
                    ]
                ]
            ]
        ];
    }

    // The emails to send out from this plugin
    public function registerMailTemplates() {
    }

    // Register front-end components
    public function registerComponents() {
        return [
           '\Monologophobia\EstateAgent\Components\Properties' => 'properties'
        ];
    }

    public function registerReportWidgets() {
    }

    public function registerSettings() {
        return [
            'estateagent' => [
                'label'       => 'Settings',
                'description' => 'Manage Estate Agent Settings.',
                'category'    => 'Estate Agent',
                'icon'        => 'icon-cog',
                'class'       => 'Monologophobia\EstateAgent\Models\Settings',
                'order'       => 400
            ]
        ];
    }

    public function boot() {
    }

    public function registerSchedule($schedule) {
    }

}
